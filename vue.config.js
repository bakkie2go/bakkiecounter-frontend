module.exports = {
  pwa: {
    name: 'Bakkiecounter',
    themeColor: '#448aff',
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      // https://stackoverflow.com/a/59389816
      navigateFallback: 'index.html'
    },
    manifestPath: 'manifest.json',
    manifestOptions: {
      name: 'Bakkiecounter',
      description: 'The Bakkiecounter',
      short_name: 'Bakkiecounter',
      start_url: '/',
      theme_color: '#448aff',
      background_color: '#424242',
      display: 'standalone',
      orientation: 'portrait',
      icons: [
        {
          src: '/img/icons/android-chrome-192x192.png',
          sizes: '192x192',
          type: 'image/png',
          purpose: 'maskable any'
        },
        {
          src: '/img/icons/android-chrome-512x512.png',
          sizes: '512x512',
          type: 'image/png',
          purpose: 'maskable any'
        }
      ]
    },
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black'
  },
  // vue-cli-plugin-compression
  pluginOptions: {
    compression: {
      brotli: {
        filename: '[path].br[query]',
        algorithm: 'brotliCompress',
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        compressionOptions: {
          level: 11
        },
        minRatio: 0.8
      },
      gzip: {
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        minRatio: 0.8
      }
    },
    transpileDependencies: [
      'vuetify'
    ]
  }
}
