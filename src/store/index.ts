import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      id: undefined,
      mail: undefined,
      name: undefined,
      active: undefined
    },
    bakkies: -1
  },
  mutations: {
    updateUserId (state, payload) {
      state.user.id = payload
    },
    updateUser (state, payload) {
      state.user = payload
    },
    decrementBakkies (state) {
      state.bakkies--
    },
    incrementBakkies (state) {
      state.bakkies++
    },
    setBakkies (state, payload) {
      state.bakkies = payload
    }
  },
  actions: {
  },
  modules: {
  }
})
