import axios, { AxiosInstance } from 'axios'

const apiClient: AxiosInstance = axios.create({
  baseURL: 'https://bakkiecounter.com/api',
  headers: {
    'Content-type': 'application/json'
  }
})

export default apiClient
