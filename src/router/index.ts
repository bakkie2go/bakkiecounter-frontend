import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'NotLoggedIn',
    component: () => import(/* webpackChunkName: "notloggedin" */ '../views/NotLoggedInPage.vue')
  },
  {
    path: '/account/',
    name: 'Account',
    component: () => import(/* webpackChunkName: "account" */ '../views/AccountPage.vue')
  },
  {
    path: '/bakkies/',
    name: 'Bakkies',
    component: () => import(/* webpackChunkName: "bakkies" */ '../views/BakkiesPage.vue')
  },
  {
    path: '/log/',
    name: 'Log',
    component: () => import(/* webpackChunkName: "friends" */ '../views/LogPage.vue')
  },
  {
    path: '/friends/',
    name: 'Friends',
    component: () => import(/* webpackChunkName: "friends" */ '../views/FriendsPage.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
