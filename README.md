# Bakkiecounter Frontend

This repository hosts the code of a simple hobby project to track one's coffee consumption.
If you like this idea, then either clone the repository and host it yourself, or use our public instance on [bakkiecounter.com](https://bakkiecounter.com). The backend can be found [here](https://codeberg.org/bakkie2go/bakkiecounter-backend).

Software stack:

* [VueJS](https://vuejs.org/) as the JS framework
* [VuetifyJS](https://vuetifyjs.com/) to provide material design components

## Getting started

The documentation can be found on [docs.bakkiecounter.com](https://docs.bakkiecounter.com)
